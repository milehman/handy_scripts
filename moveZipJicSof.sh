#!/bin/bash

if [ $# -eq 0 ]; then
    >&1 echo "1st arg: path to project root. 2nd arg: desired directory location of compressed directory. 3rd arg: repo name\n"
    exit 1
elif [ $# -lt 3 ]; then
    >&2 echo "Too few arguments. 1st arg: path to project root. 2nd arg: desired directory location of compressed directory. 3rd arg: repo name\n"
    exit 1
elif [ $# -gt 3 ]; then
    >&2 echo "Too many arguments. 1st arg: path to project root. 2nd arg: desired directory location of compressed directory. 3rd arg: repo name\n"
    exit 1
fi

cmHsh=$(git rev-parse HEAD)
brNm=$(git rev-parse --abbrev-ref HEAD)
todaysDate=$(date +"%m%d%Y")
timeHourMin=$(date +"%H%M")
brNmPlusDateTime="${brNm}_${todaysDate}_${timeHourMin}"

projectRoothPath=$1
echo "Project root path given: ${1}"

destPath=$2
echo "Destination path given: ${2}"

echo "Repo name given: ${3}"

newDir=$destPath/$brNmPlusDateTime/$cmHsh/
echo "Path of folder containing jic and sof: ${newDir}"

mkdir -p $newDir

cp $1/quartus/lib_top_$3/top_$3.sof $newDir/
#need a more generic way to get jic
cp $1/jicbuilder/lib_top_$3/63491732.jic $newDir/

#find stp files and put them in the new dir too
find . -name  \*.stp -exec cp {} $newDir \;
